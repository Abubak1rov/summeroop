package ;

import java.sql.*;
import lombok.Data;

@Data
public class Accessories {
    String url = "jdbc:mysql://localhost:3306/aitu";
    String username = "";
    String password = "";

    private int id;
    private String name;
    private int amount;
    private double price;
    private int fish_id;
    private double total = 0;



    public void showTable() {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM accessories");

            while(resultSet.next()) {
                Accessories accessories = new Accessories();
                accessories.setId(resultSet.getInt("id"));
                accessories.setName(resultSet.getString("name"));
                accessories.setAmount(resultSet.getInt("amount"));
                accessories.setPrice(resultSet.getDouble("cost"));
                accessories.setFish_id(resultSet.getInt("fish_id"));

                System.out.println(accessories);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void totalCost() {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            Fish fish = new Fish();
            ResultSet resultSet = statement.executeQuery("SELECT accessories.id, accessories.name, accessories.amount, " +
                    "accessories.cost, accessories.fish_id, (fish.amount * fish.cost + accessories.amount * accessories.cost)"  +
                    "AS total FROM accessories INNER JOIN fish ON accessories.fish_id = fish.id");
            double sum = 0;
            while (resultSet.next()) {
                Accessories accessories = new Accessories();
                accessories.setId(resultSet.getInt("id"));
                accessories.setName(resultSet.getString("name"));
                accessories.setAmount(resultSet.getInt("amount"));
                accessories.setPrice(resultSet.getDouble("cost"));
                accessories.setFish_id(resultSet.getInt("fish_id"));
                accessories.setTotal(resultSet.getDouble("total"));


                System.out.println(accessories);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void totalCostByAccessorieId(int id) {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            Fish fish = new Fish();
            ResultSet resultSet = statement.executeQuery("SELECT accessories.id, accessories.name, accessories.amount, " +
                    "accessories.cost, accessories.fish_id, (fish.amount * fish.cost + accessories.amount * accessories.cost)"  +
                    "AS total FROM accessories INNER JOIN fish ON accessories.fish_id = fish.id WHERE accessories.id = " + id);
            double sum = 0;
            while (resultSet.next()) {
                Accessories accessories = new Accessories();
                accessories.setId(resultSet.getInt("id"));
                accessories.setName(resultSet.getString("name"));
                accessories.setAmount(resultSet.getInt("amount"));
                accessories.setPrice(resultSet.getDouble("cost"));
                accessories.setFish_id(resultSet.getInt("fish_id"));
                accessories.setTotal(resultSet.getDouble("total"));


                System.out.println(accessories);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public String toString() {
        Fish fish = new Fish();
        return "Accessories [" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", cost=" + this.price +
                ", fish_id=" + fish_id +
                ", total=" + total +
                ']';
    }
}
