package ;

import java.sql.*;

public class Aquarium extends Accessories{

    String url = "jdbc:mysql://localhost:3306/aitu";
    String username = "";
    String password = "";

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getAmount() {
        return super.getAmount();
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    }

    @Override
    public int getFish_id() {
        return super.getFish_id();
    }

    @Override
    public double getTotal() {
        return super.getTotal();
    }

    public void totalCost() {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            Fish fish = new Fish();
            ResultSet resultSet = statement.executeQuery("SELECT accessories.id, accessories.name, accessories.amount, " +
                    "accessories.cost, accessories.fish_id, (fish.amount * fish.cost + accessories.amount * accessories.cost)"  +
                    "AS total FROM accessories INNER JOIN fish ON accessories.fish_id = fish.id");
            double sum = 0;
            while (resultSet.next()) {
                Aquarium aquarium = new Aquarium();
                aquarium.setId(resultSet.getInt("id"));
                aquarium.setName(resultSet.getString("name"));
                aquarium.setAmount(resultSet.getInt("amount"));
                aquarium.setPrice(resultSet.getDouble("cost"));
                aquarium.setFish_id(resultSet.getInt("fish_id"));
                aquarium.setTotal(resultSet.getDouble("total"));


                System.out.println(aquarium);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        Fish fish = new Fish();
        return "Aquarium [" +
                "Accessories.id=" + getId() +
                ", Accessories.name='" + getName() + '\'' +
                ", Accessories.amount=" + getAmount() +
                ", Accessories.cost=" + getPrice() +
                ", Fish.id=" + getFish_id() +
                ", total=" + getTotal() +
                ']';
    }
}
