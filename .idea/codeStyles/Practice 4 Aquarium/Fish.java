package ;

import java.sql.*;
import lombok.Data;

@Data
public class Fish extends Accessories{
    String url = "jdbc:mysql://localhost:3306/aitu";
    String username = "";
    String password = "";


    private int id;
    private String name;
    private double cost;
    private int amount;
    private double total=0.0;


    @Override
    public void showTable() {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM fish");

            while (resultSet.next()) {
                Fish fish = new Fish();
                fish.setId(resultSet.getInt("id"));
                fish.setName(resultSet.getString("name"));
                fish.setAmount(resultSet.getInt("amount"));
                fish.setPrice(resultSet.getInt("cost"));

                System.out.println(fish);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void totalCost() {
        try {
            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT id, name, amount, cost, (amount * cost) AS total FROM fish");


            while (resultSet.next()) {
                Fish fish = new Fish();
                fish.setId(resultSet.getInt("id"));
                fish.setName(resultSet.getString("name"));
                fish.setAmount(resultSet.getInt("amount"));
                fish.setPrice(resultSet.getInt("cost"));
                fish.setTotal(resultSet.getDouble("total"));


                System.out.println(fish);
            }
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Fish [" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cost=" + cost +
                ", amount=" + amount +
                ", total=" + total +
                ']';
    }
}
