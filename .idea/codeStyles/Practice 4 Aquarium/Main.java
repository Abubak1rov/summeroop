package ;

public class Main {
    public static void main(String[] args) {

        Aquarium aquarium = new Aquarium();
        aquarium.totalCost();
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

        Accessories accessories = new Accessories();
        accessories.showTable();
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        accessories.totalCostByAccessorieId(1);
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

        Fish fish = new Fish();
        fish.showTable();
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        fish.totalCost();
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }
}

